/**
 * Created by tonyarcher on 8/24/16.
 */

var https = require('https'),
    log = require('@zetta/log-colors'),
    argv = require('yargs').argv;

const bbAPI = 'https://api.bitbucket.org/2.0/';

if (typeof argv.u !== 'undefined') {
    log.good('got ' + argv.u);
} else {
    log.bad('no -u param');
}